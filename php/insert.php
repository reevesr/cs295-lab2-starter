<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
    </head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1>        
	        
				<h2>Thank you for submitting <em><?php echo htmlentities($_POST["title"]); ?><em></h2>

				<h3>Ingredients</h3>
				<div><?php echo htmlentities($_POST["ingredient0"]); ?></div>
				<div><?php echo htmlentities($_POST["ingredient1"]); ?></div>
				<div><?php echo htmlentities($_POST["ingredient2"]); ?></div>

				<h3>Instructions</h3>
				<div><?php echo htmlentities($_POST["instructions"]); ?></div>

		</div>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
